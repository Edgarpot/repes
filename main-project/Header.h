#ifndef BOOK_SUBSCRIPTION_H
#define BOOK_SUBSCRIPTION_H

#include "header1.h"

struct date
{
    int day;
    int month;
    int year;
    int time;
};

struct plate
{
    char vid[MAX_STRING_SIZE];
    char chet[MAX_STRING_SIZE];
    char summa[MAX_STRING_SIZE];
    char naznacheni[MAX_STRING_SIZE];
};

struct book_subscription
{
    plate reader;
    date start;
    date finish;
    plate author;
    char title[MAX_STRING_SIZE];
};

#endif
